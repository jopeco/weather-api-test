package com.weather.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.weather.component.Weather;
import com.weather.current.component.WeatherCurrent;
import com.weather.service.impl.WeatherCurrentServiceImpl;
import com.weather.service.impl.WeatherServiceImpl;

@RestController
@RequestMapping("/weather")
public class WeatherController {
	
	private static final Logger log = Logger.getLogger(WeatherController.class);
	
	@Value("${app.appid}")
	private String appid;
	

	
	@Autowired
	@Qualifier("WeatherService")
	private WeatherServiceImpl weatherService;
	
	@Autowired
	@Qualifier("CurrentWeatherService")
	private WeatherCurrentServiceImpl currentService;
	
	@GetMapping("/get/{lat}/{lon}/{key}")
	@ResponseBody
	public Weather getWeather(@PathVariable("lat") double latitud, 
			@PathVariable("lon") double longitud, 
			@PathVariable("key") String keyapi) {
		
		if (keyapi.equals(appid)) {
			Weather weather=weatherService.getWeatherServiceNow(latitud, longitud);
			return weather;
		}else {
			return null;
		}

	}
	@GetMapping("/current/{lat}/{lon}/{key}")
	@ResponseBody
	public WeatherCurrent getCurrentWeather(@PathVariable("lat") double latitud, 
			@PathVariable("lon") double longitud, 
			@PathVariable("key") String keyapi) {
		
		if (keyapi.equals(appid)) {
			WeatherCurrent weather=currentService.getCurrentWeather(latitud, longitud);
			return weather;
		}else {
			return null;
		}
	}
	
}
