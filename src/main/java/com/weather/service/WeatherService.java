package com.weather.service;

import com.weather.component.Weather;

public interface WeatherService{

	public Weather getWeatherServiceNow(double lat, double lon);
	
}
