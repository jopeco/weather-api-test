package com.weather.service.impl;


import java.net.URI;
import java.net.URISyntaxException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.weather.component.Weather;
import com.weather.service.WeatherService;

@Service("WeatherService")
public class WeatherServiceImpl implements WeatherService{
	
	private static final Logger log = Logger.getLogger(WeatherServiceImpl.class);
	String urlService;
	
	@Value("${app.urlWeatherApi}")
	private String urlWeatherApi;
	
	@Value("${app.appid}")
	private String appid;
	
	private Weather weather = null;
	
	@Override
	public Weather getWeatherServiceNow(double lat, double lon) {
		// TODO Auto-generated method stub
		
		log.info("****WeatherService: Inicio");
		RestTemplate rest_temp=new RestTemplate();
		
		
		log.info("Url service prop "+urlWeatherApi);
		log.info("api key prop "+appid);
		log.info("lat "+lat);
		log.info("lon "+lon);
		
		urlService=urlWeatherApi;
		urlService+="lat="+lat;
		urlService+="&lon="+lon;
		urlService+="&units=metric";
		urlService+="&appid="+appid;
		urlService+="&type=accurate";
		
		
		
		try {
			URI urlWS=new URI(urlService);
			log.info("**URL service "+urlService);
			
			weather = rest_temp.getForObject(urlWS, Weather.class);
			log.info("******Respuesta Service: "+weather.toString());
			
			
			
		} catch (RestClientException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			log.error("ERROR SERVICE "+e.getMessage());
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			log.error("ERROR2 SERVICE "+e.getMessage());
		}
		log.info("****WeatherService: Fin");
		return weather;
	}


}
