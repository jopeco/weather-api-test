package com.weather.service.impl;

import java.net.URI;
import java.net.URISyntaxException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.weather.current.component.WeatherCurrent;
import com.weather.service.WeatherCurrentService;

@Service("CurrentWeatherService")
public class WeatherCurrentServiceImpl implements WeatherCurrentService {

	private static final Logger log = Logger.getLogger(WeatherServiceImpl.class);
	String urlService;
	
	@Value("${app.urlCurrentWeatherApi}")
	private String urlWeatherApi;
	
	@Value("${app.appid}")
	private String appid;
	
	private WeatherCurrent weather = null;
	
	@Override
	public WeatherCurrent getCurrentWeather(double latitud, double longitud) {
		// TODO Auto-generated method stub
		log.info("****WeatherService: Inicio");
		RestTemplate rest_temp=new RestTemplate();
		
		
		log.info("Url service prop "+urlWeatherApi);
		log.info("api key prop "+appid);
		log.info("lat "+latitud);
		log.info("lon "+longitud);
		
		urlService=urlWeatherApi;
		urlService+="lat="+latitud;
		urlService+="&lon="+longitud;
		urlService+="&units=metric";
		urlService+="&appid="+appid;
		urlService+="&type=accurate";
		
		
		
		try {
			URI urlWS=new URI(urlService);
			log.info("**URL service "+urlService);
			
			weather = rest_temp.getForObject(urlWS, WeatherCurrent.class);
			log.info("******Respuesta Service: "+weather.toString());
			
			
			
		} catch (RestClientException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			log.error("ERROR SERVICE "+e.getMessage());
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			log.error("ERROR2 SERVICE "+e.getMessage());
		}
		log.info("****WeatherService: Fin");
		
		return weather;
	}
	

}
