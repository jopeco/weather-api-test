package com.weather.service;

import com.weather.current.component.WeatherCurrent;

public interface WeatherCurrentService {
	
	public WeatherCurrent getCurrentWeather(double latitud, double longitud);

}
